SET GLOBAL general_log = 1;
SET GLOBAL log_output = 'table';

SELECT * FROM mysql.general_log
WHERE  event_time  > (NOW() - INTERVAL 10 SECOND)
;

-- TRUNCATE TABLE mysql.general_log;
-- SET GLOBAL general_log = 1;
