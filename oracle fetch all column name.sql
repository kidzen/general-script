create view all_col_name as 
with ii_t as (
  select distinct ',ii.'||column_name||' ii_'||column_name as colName
  from all_tab_columns
  where table_name = 'INVENTORY_ITEMS'
)
, oi_t as (
  select distinct ',oi.'||column_name||' oi_'||column_name as colName
  from all_tab_columns
  where table_name = 'ORDER_ITEMS'
)
, o_t as (
  select distinct ',o.'||column_name||' o_'||column_name as colName
  from all_tab_columns
  where table_name = 'ORDERS'
)
, ic_t as (
  select distinct ',ic.'||column_name||' ic_'||column_name as colName
  from all_tab_columns
  where table_name = 'INVENTORIES_CHECKIN'
)
, i_t as (
  select distinct ',i.'||column_name||' i_'||column_name as colName
  from all_tab_columns
  where table_name = 'INVENTORIES'
)
, c_t as (
  select distinct ',c.'||column_name||' c_'||column_name as colName
  from all_tab_columns
  where table_name = 'CATEGORIES'
)
, t_t as (
  select distinct ',t.'||column_name||' t_'||column_name as colName
  from all_tab_columns
  where table_name = 'TRANSACTIONS'
)
, all_t as (
  select colName from ii_t
  union ALL
  select colName from oi_t
  union ALL
  select colName from o_t
  union ALL
  select colName from ic_t
  union ALL
  select colName from i_t
  union ALL
  select colName from c_t
  union ALL
  select colName from t_t
)

select * from all_t
;
